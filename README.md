# Game Of Life

Django + Celery test

En el siguiente ejercicio deberá ejecutar tareas de Celery, esta es una prueba que intenta validar los conocimientos de Django y Celery, principalmente en el llamado de tareas.

#Reto
Debe implementar un Juego de la Vida desde 0 de un tamaño especificado por el usuario, configurando el proyecto y celery.
Las reglas del juego son las siguientes:
* Si una célula está viva y tiene dos o tres vecinas vivas, sobrevive.
* Si una célula está muerta y tiene tres vecinas vivas, nace.
* Si una célula está viva y tiene más de tres vecinas vivas, muere. 

La aplicación debe tener una tarea que se comporte como una iteración del juego y debe ser llamada múltiples veces durante la duración del juego, además debe ser capaz de recibir args y kwargs.
La aplicación debe tener al menos tres tareas que modifiquen el estado de la matriz.
Ideas que podrían servir: 
* Una tarea que limpie el tablero
* Una tarea que escoja al azar posiciones de la matriz y cambie el estado de la casilla por el contrario.
* Una tarea que ponga el estado viva a todas las casillas múltiples de 3.

Las tareas modificadoras deben poder ser lanzadas con retraso usando un tiempo calculado con random en un rango de tiempo especificado por el usuario, además debe de usar tanto countdown como ETA (no al mismo tiempo).
La ejecución de la tarea no puede durar más de 5 segundos.

Debe crear una tarea que se ejecute cada minuto y que llame a alguna de las 3 tareas desarrolladas.

Debe crear una una interfaz web que permita iniciar el juego, y especificar el tamaño de la matriz, el rango de tiempo entre una iteración y otra, en esta pantalla debe mostrar una imagen del estado del juego que se refresque cada 30 segundos (sin refrescar la página).
Además debe crear un botón que permita pausar el juego, cuando el juego esté pausado ninguna tarea debe modificar la matriz del juego o ejecutarse como tal.
Aspectos a considerar:
* Se calificará el manejo de git.
* Se calificará el código del repositorio como producto final, por lo que se recomienda analizar y revisar bien el código antes de considerar finalizada la prueba.
* Puede usar librerías tipo matlibplot para generar la imagen de la matriz.
* Haga un pequeño documento que le permita entender el problema a resolver.
* No es válido tener una tarea que corra al infinito, la idea es que manejen las iteraciones con llamados a tareas.
* Puede crear los modelos necesarios para mantener la información de la aplicación.

Referencias:
* https://www.youtube.com/watch?v=qPtKv9fSHZY
* https://docs.celeryproject.org/en/stable/userguide/calling.html#guide-calling
* https://es.wikipedia.org/wiki/Juego_de_la_vida
