import datetime

from django.contrib import messages
from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponse
from django.shortcuts import render
import numpy as np
from django.utils import timezone
from django.views.decorators.http import require_http_methods

from gameoflifeapp.forms import GameForm
from gameoflifeapp.models import GameSession
from gameoflifeapp.tasks import game_iteration
from gameoflifeapp.utils import get_matrix_uri


def home(request):
    return render(request, "home.html", {
        'form': GameForm()
    })


@require_http_methods(["POST"])
def start_game(request):
    form = GameForm(request.POST or None)
    if form.is_valid():
        game = form.save(commit=False)
        matrix = np.random.randint(2, size=(game.matrix_size, game.matrix_size))
        # Save matrix state in DB
        game.matrix_field = game.get_binary_matrix(matrix)
        game.save()
        # Get matrix drawing
        matrix_uri = get_matrix_uri(matrix)
        # Start task game iteration
        game_iteration.delay(game.id)
        return JsonResponse({
            'id': game.id,
            'uri': matrix_uri,
        })
    else:
        return JsonResponse({
            'form': form,
        })


@require_http_methods(["GET"])
def get_board(request, id):
    game = GameSession.objects.get(id=id)
    matrix = game.get_matrix()
    matrix_uri = get_matrix_uri(matrix)
    return JsonResponse({
        'uri': matrix_uri
    })


@require_http_methods(["PUT"])
def change_gamestate(request, id):
    game = GameSession.objects.get(id=id)
    game.state = not game.state
    game.save()
    if game.state:
        game_iteration.apply_async((game.id,))
    return JsonResponse({
        "id": game.id,
        "state": game.state
    })
