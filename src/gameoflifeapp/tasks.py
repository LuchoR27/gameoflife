import datetime
import random
import time

import numpy as np
from celery.utils.log import get_task_logger
from django.core.cache import cache
from django.utils import timezone

from gameoflife.celery import app
from gameoflifeapp.models import GameSession

logger = get_task_logger('ejemploceleryapp')


def lock(game_id, timeout):
    locked = cache.add(game_id, 'game_lock', timeout)
    while not locked:
        time.sleep(3)
        locked = cache.add(game_id, 'game_lock', game_id, timeout)
    logger.info(f"Locked game {game_id}.")


def unlock(game_id):
    logger.info(f"Unlocked game {game_id}.")
    cache.delete(game_id)


@app.task()
def game_iteration(game_id):
    game = GameSession.objects.get(id=game_id)
    # If game is not paused
    lock(game.id, game.max_itertime)
    if game.state:
        logger.info(f"Iterating game {game.id}.")
        matrix = game.get_matrix()
        new_matrix = np.copy(matrix)
        n = len(matrix)
        for x in range(0, n):
            for y in range(0, n):
                neighbors = matrix[(x - 1) % n][(y - 1) % n] + \
                            matrix[x % n][(y - 1) % n] + \
                            matrix[(x + 1) % n][(y - 1) % n] + \
                            matrix[(x + 1) % n][y % n] + \
                            matrix[(x - 1) % n][y % n] + \
                            matrix[(x - 1) % n][(y + 1) % n] + \
                            matrix[x % n][(y + 1) % n] + \
                            matrix[(x + 1) % n][(y + 1) % n]
                if matrix[x][y] == 0 and neighbors == 3:
                    new_matrix[x][y] = 1
                elif matrix[x][y] == 1 and neighbors > 3:
                    new_matrix[x][y] = 0
        game.matrix_field = game.get_binary_matrix(new_matrix)
        game.save()
        # Queue next iteration
        eta_or_countdown = random.randint(0, 1)
        time = random.randint(game.min_itertime, game.max_itertime)
        logger.info(f"Next iteration will be in {time} seconds.")
        if eta_or_countdown == 0:
            game_iteration.apply_async((game.id,), eta=timezone.now() + datetime.timedelta(seconds=time))
        else:
            game_iteration.apply_async((game.id,), countdown=time)
    unlock(game.id)

@app.task()
def game_modifier():
    games = GameSession.objects.filter(state=True)
    for game in games:
        lock(game.id, game.max_itertime)
        option = random.randint(0, 9)
        logger.info(f"Game {game.id} is being modified.")
        if 0 <= option <= 3:
            switch_odd(game.id)
        elif 4 <= option <= 7:
            switch_even(game.id)
        elif 8 <= option <= 9:
            switch_all(game.id)
        unlock(game.id)


# Task that switch odd column and row number cells
@app.task()
def switch_odd(game_id):
    game = GameSession.objects.get(id=game_id)
    logger.info(f"Game {game.id} is switching odd spaces.")
    matrix = game.get_matrix()
    n = len(matrix)
    for x in range(1, n, 2):
        for y in range(1, n, 2):
            if matrix[x][y] == 0:
                matrix[x][y] = 1
            elif matrix[x][y] == 1:
                matrix[x][y] = 0
    game.matrix_field = game.get_binary_matrix(matrix)
    game.save()


# Task that switch even column and row number cells
@app.task()
def switch_even(game_id):
    game = GameSession.objects.get(id=game_id)
    logger.info(f"Game {game.id} is switching even spaces.")
    matrix = game.get_matrix()
    n = len(matrix)
    for x in range(0, n, 2):
        for y in range(0, n, 2):
            if matrix[x][y] == 0:
                matrix[x][y] = 1
            elif matrix[x][y] == 1:
                matrix[x][y] = 0
    game.matrix_field = game.get_binary_matrix(matrix)
    game.save()


# Task that switch all cells of the matrix
@app.task()
def switch_all(game_id):
    game = GameSession.objects.get(id=game_id)
    logger.info(f"Game {game.id} is switching all spaces.")
    inverted_matrix = 1 - game.get_matrix()
    game.matrix_field = game.get_binary_matrix(inverted_matrix)
    game.save()
