document.addEventListener('DOMContentLoaded', function() {
    let id;
    let state = true;
    let board = document.getElementById('board');
    let state_btn = document.getElementById('state-btn');
    let form = document.getElementById('form');
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    board.style.display = "none";
    state_btn.style.display = "none";
    state_btn.addEventListener("click", change_gamestate);
    form.addEventListener("submit", e => {
        e.preventDefault();
        let data = new FormData(form);
        fetch('/start_game', {
                method: 'POST',
                body: data,
            })
            .then(response => response.json())
            .then(data => {
                //Hide the form and show the game
                form.style.display = "none";
                board.style.display = "block";
                state_btn.style.display = "block";
                id = data.id;
                board.src = data.uri;
                board.className = "mx-auto d-block"
                //Set the reloading to 30 seconds
                setInterval(reload_board, (30 * 1000));
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    })

    function change_gamestate(){
        fetch(`/change_gamestate/${id}`, {
                method: 'PUT',
            })
            .then(response => response.json())
            .then(data => {
                // True: Game is running, False: Game is paused
                console.log(data);
                state = data.state;
                state ? console.log("¡Resumed!") : console.log("¡Paused!");
                state_btn.innerHTML = state ? "Pause" : "Resume";
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function reload_board(){
        console.log("¡Reloading!")
        fetch(`/get_board/${id}`, {
                method: 'GET',
                headers: new Headers({'cache-control': 'no-store'}),
            })
            .then(response => response.json())
            .then(data => {
                board.src = data.uri;
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }
});

