from django.contrib import admin
from gameoflifeapp.models import GameSession

admin.site.register(GameSession)
