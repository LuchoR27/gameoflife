import base64
import pickle

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class GameSession(models.Model):
    matrix_field = models.BinaryField()
    matrix_size = models.IntegerField(null=True, validators=[MinValueValidator(3), MaxValueValidator(300)])
    min_itertime = models.IntegerField(null=True, validators=[MinValueValidator(0)])
    max_itertime = models.IntegerField(null=True)
    state = models.BooleanField(default=True)

    def get_binary_matrix(self, matrix):
        np_bytes = pickle.dumps(matrix)
        return base64.b64encode(np_bytes)

    def get_matrix(self):
        np_bytes = base64.b64decode(self.matrix_field)
        return pickle.loads(np_bytes)
