from django import forms

from gameoflifeapp.models import GameSession


class GameForm(forms.ModelForm):
    class Meta:
        model = GameSession
        fields = ['matrix_size', 'min_itertime', 'max_itertime']
