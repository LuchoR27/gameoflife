import base64
import io
import urllib
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

def get_matrix_uri(matrix):
    cmap = ListedColormap(['k', 'w'])
    fig, ax = plt.subplots()
    ax.matshow(matrix, cmap=cmap)
    fig = plt.gcf()
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    plt.close(fig)
    return 'data:image/png;base64,' + urllib.parse.quote(string)
