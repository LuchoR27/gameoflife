from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from gameoflifeapp import views

urlpatterns = [
    path('', views.home, name="home"),
    path('start_game', views.start_game, name="start_game"),
    path('get_board/<int:id>', views.get_board, name="get_board"),
    path('change_gamestate/<int:id>', csrf_exempt(views.change_gamestate), name="change_gamestate"),

]