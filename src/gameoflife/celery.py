import os

from celery import Celery
from gameoflife import settings
# Set the default Django settings module for the 'celery' program.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gameoflife.settings')

app = Celery('gameoflife')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    'modify-every-minute': {
        'task': 'gameoflifeapp.tasks.game_modifier',
        'schedule': 60.0,
    },
}
app.conf.timezone = settings.TIME_ZONE

# Load task modules from all registered Django apps.
app.autodiscover_tasks()