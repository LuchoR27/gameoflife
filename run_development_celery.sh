#!/bin/bash

#source venv/bin/activate

docker run -d --rm --name ecsl-mail -p 8025:8025 -p  1025:1025 -d mailhog/mailhog
docker run -d --rm --name ecsl-rabbitmq -p 5672:5672 -d rabbitmq:3
docker run -d --rm --name ecsl-memcached -p 11211:11211 -d memcached



echo "Esperando el inicio de rabbitmq ..." && sleep 20
cd src/
celery -A gameoflife worker -B -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler


docker rm -f ecsl-mail
docker rm -f ecsl-rabbitmq
docker rm -f ecsl-memcached
cd ..

